import React from 'react';
import './App.css';
import Person from './myJS';

const App = (props) => (
    <div className="App">
        <Person name="Агенты А.Н.К.Л." year="2015" src="./img/Agents_UNCLE.jpg" />
        <Person name="Ходячий замок" year="2004" src="./img/moving_castle.jpg" />
        <Person name="Доктор Стрэндж" year="2016" src="./img/Doctor_Strange.jpg" />
    </div>
);

export default App;
