import React from 'react';

function Person(props) {
    return (
        <div className="person">
            <h2>{props.name}</h2>
            <h3>Год выпуска: {props.year}</h3>
            <img src={props.src}/>
        </div>
    );
}

export default Person;